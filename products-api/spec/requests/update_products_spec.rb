require 'rails_helper'

RSpec.describe "Update products", type: :request do
  before do 
    @product = Product.create(name: "coat",
                                description:"A black leather jacket",
                                price: 169.9)
    end
  
  describe "PUT #update" do
    context "when success" do
      it "should update the product" do
        put "/products/#{@product.id}", params: { :product => { :name => "jacket" } },  headers: { "API-AUTHENTICATE" => true }

        expect(response.status).to eq(200)
        last_product = Product.last
        expect(last_product.name).to eq("jacket")
      end
    end

    context "when fail" do
      it "should not update the product" do
        put "/products/#{@product.id}", params: { :product => { :description => "" } }, headers: { "API-AUTHENTICATE" => true }

        expect(response.status).to eq(422)
        last_product = Product.last
        expect(last_product.description).to eq("A black leather jacket")
      end
    end 
  end
end