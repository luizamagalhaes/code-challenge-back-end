require 'rails_helper'

RSpec.describe "Create products", type: :request do
  context "when authenticated" do
    it "creates a new product" do
      create_params = {
        product: {
          name: "blue sandals",
          description: "a nice blue sandals with tiny details",
          price: 49.9 
        }
      }

      post "/products", params: create_params, headers: { "API-AUTHENTICATE" => true }

      expect(response.status).to eq(201)
    end

    it " invalid parameters" do
      create_params = {
        product: {
          name: "",
          description: "a nice boots with spikes",
          price: 149.9
        }
      }

      post "/products", params: create_params, headers: { "API-AUTHENTICATE" => true }

      body = JSON.parse(response.body)

      expect(response.status).to eq(422)
      last_product = Product.last
      expect(last_product).to be_nil
      expect(body).to include("Name can't be blank")
    end

    it "initial status should be inactive" do
      create_params = {
        product: {
          name: "blue sandals",
          description: "a nice blue sandals with tiny details",
          price: 49.9
        }
      }

      post "/products", params: create_params, headers: { "API-AUTHENTICATE" => true }

      expect(response.status).to eq(201)

      last_product = Product.last
      expect(last_product.status).to eq("inactive")
     end
  end

  context "when not authenticated" do
    
    it " with valid parameters" do
      create_params = {
        product: {
          name: "boots",
          description: "a nice boots with spikes",
          price: 49.9
        }
      }

      post "/products", params: create_params

      body = JSON.parse(response.body)

      expect(response.status).to eq(401)
      last_product = Product.last
      expect(last_product).to be_nil
      expect(body).to eq({"error" => "Access denied"})
    end
  end
end