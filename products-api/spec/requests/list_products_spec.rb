require 'rails_helper'

RSpec.describe "List of products", type: :request do
  
  context "when have products" do
    before do 
      @product1 = Product.create(name: "coat",
                                description:"A black leather coat",
                                price: 169.9)

      @product2 = Product.create(name: "sneakers",
                                description:"A black leather sneakers",
                                price: 269.9)  
    end
   
    it "show a products list" do
      get "/products"

      body = JSON.parse(response.body)

      expect(body).to be_a(Array)
      expect(body.count).to eq(2)
      expect(response.status).to eq(200)
    end       
  end

  context "when doest have products" do
    it "shows an empty products list" do
      get "/products"

      body = JSON.parse(response.body)

      expect(body).to be_a(Array)
      expect(body.count).to eq(0)
      expect(response.status).to eq(200)
    end
  end
end