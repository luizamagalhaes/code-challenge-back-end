require 'rails_helper'

RSpec.describe "Activate products", type: :request do
  context "when success" do
    before do 
      @product1 = Product.create(name: "coat",
                                description:"A black leather coat",
                                price: 169.9,
                                status: "inactive")
    end

    it "activates a product" do
      put "/products/#{@product1.id}/activate", headers: { "API-AUTHENTICATE" => true }
      expect(response.status).to eq(200)

      last_product = Product.last
      expect(last_product.status).to eq("activated")
    end
  end
end