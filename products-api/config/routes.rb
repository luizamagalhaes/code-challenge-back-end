Rails.application.routes.draw do
  resources :products, only: [:create, :index, :update]
  resources :products do
    put 'activate', on: :member
  end
end
