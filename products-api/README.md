## Code Challenge backend
ProductAPI is a project to create, list, update and activate products.

### System requirements:
* Ruby version 2.3.3
* Rails version 5.0.2

### Installation

Clone the project:
`$ git clone https://luizamagalhaes@bitbucket.org/luizamagalhaes/code-challenge-back-end.git`

Run:
`$bundle install`

To run the suite of test : `$ rspec`

To create a Database: `$ rails db:create`

To run the migrations: `$ rails db:migrate`

The product structure is:
* name;
* description;
* price;
* status

### ENDPOINTS:
`GET "/products"` => Products list

`POST "/products"` => Create a new product
*Parameters:*
```json
{ 
    "name" => "Ball", 
    "description" => "soccer ball", 
    "price" => 10.0 
}
```

`PUT "/products/:id"` => Update a product

`PUT "/products/:id/activate"` => Activate the product