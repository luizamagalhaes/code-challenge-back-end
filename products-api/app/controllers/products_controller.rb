class ProductsController < ApplicationController
  before_action :verify_access, except: [:index]

  def index
    products = Product.all
    render json:products, status: :ok 
   end

  def create
    product = Product.new(product_params)
    
    if product.save
      render json:product, status: :created
    else
      render json:product.errors.full_messages, status: :unprocessable_entity
    end
  end

  def update
    product = Product.find(params[:id])

    if product.update(product_params)
      render json:product, status: :ok
    else
      render json:product.errors.full_messages, status: :unprocessable_entity
    end  
  end

  def activate
    product = Product.find(params[:id])
    product.status = "activated"
    product.save

    render json:product, status: :ok
  end

  private

  def product_params
    params.require(:product).permit(:name, :description, :price)
  end

  def verify_access
    unless request.headers["HTTP_API_AUTHENTICATE"]
      render json:{error: "Access denied"}, status: :unauthorized
    end 
  end
end